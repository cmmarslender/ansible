FROM hashicorp/vault:latest as vault
FROM alpine:latest

COPY --from=vault /bin/vault /bin/vault
RUN apk add --no-cache ansible git openssh py3-jmespath rsync
